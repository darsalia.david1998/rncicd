import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const App = () => {
  return (
    <View style={styles.container} testID="app-container">
      <Text style={styles.text}>Lingwing</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 33,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 35,
    marginBottom: '10%',
  },
});

export default App;
