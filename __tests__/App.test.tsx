import React from 'react';
import {render} from '@testing-library/react-native';
import App from '../App';

describe('<App />', () => {
  it('should render without app crashing ...', () => {
    const {getByTestId} = render(<App />);
    expect(getByTestId('app-container')).toBeDefined();
  });

  it('should display "Lingwing" text', () => {
    const {getByText} = render(<App />);
    expect(getByText('Lingwing')).toBeDefined();
  });
});
