import React from 'react';
import {Button, View, StyleSheet} from 'react-native';
import {HelperText, TextInput} from 'react-native-paper';
import {z} from 'zod';
import {Controller, useForm} from 'react-hook-form';
import {zodResolver} from '@hookform/resolvers/zod';

const loginSchema = z.object({
  email: z.string().email('Invalid email format'),
  secretKey: z.string().min(10, 'Minimum length should be 10'),
  password: z.string().min(8, 'Minimum length should be 8'),
});

type IFormInput = z.infer<typeof loginSchema>;

export const LoginForm = () => {
  const {
    control,
    handleSubmit,
    formState: {errors, touchedFields},
  } = useForm<IFormInput>({
    resolver: zodResolver(loginSchema),
  });

  const onSubmit = (data: IFormInput) => {
    console.log(data);
  };

  const inputNames: (keyof IFormInput)[] = ['email', 'secretKey', 'password'];

  return (
    <View style={styles.container}>
      {inputNames.map(name => (
        <Controller
          key={name}
          control={control}
          name={name}
          render={({field: {onChange, onBlur, value}}) => (
            <>
              <TextInput
                label={name}
                style={[
                  styles.input,
                  touchedFields[name] && styles.touchedInput,
                ]}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
              <HelperText type="error" visible={!!errors[name]}>
                {errors[name]?.message}
              </HelperText>
            </>
          )}
        />
      ))}
      <Button title={'Submit'} onPress={handleSubmit(onSubmit)} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  input: {
    width: '90%',
  },
  touchedInput: {
    borderWidth: 1,
    borderColor: 'dodgerblue',
  },
});
